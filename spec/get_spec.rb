describe 'GET pet findByStatus with status parameter' do
  let(:path) { "pet/findByStatus?status=#{status}" }
  let(:response) { PetStoreApi.get(path: path) }

  context 'with available status' do
    let(:status) { 'available' }

    it 'makes a GET request' do
      expect(response.code).to eq(200)
    end
  end
end
