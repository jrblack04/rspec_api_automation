require_relative 'api'

# PetStoreApi extends Api
class PetStoreApi < Api
  @base_uri = 'https://petstore.swagger.io/v2'
end
