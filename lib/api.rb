require 'httparty'

# Base class Api for main HTTP methods
class Api
  def self.default_headers
    {
      content_type: 'application/json',
      accept: 'application/json'
    }
  end

  def self.get(path:, headers: {})
    HTTParty.get(
      "#{@base_uri}/#{path}",
      headers.merge(default_headers)
    )
  end

  def self.post(path:, body:, headers: {})
    HTTParty.post(
      "#{@base_uri}/#{path}",
      body.to_json,
      headers.merge(default_headers)
    )
  end

  def post_non_json(path:, body:, headers: {})
    HTTParty.post(
      "#{@base_uri}/#{path}",
      body,
      headers.merge(default_headers)
    )
  end

  def put(path:, body:, headers: {})
    HTTParty.put(
      "#{@base_uri}/#{path}",
      body,
      headers.merge(default_headers)
    )
  end

  def delete(path:, body:, headers: {})
    HTTParty.delete(
      "#{@base_uri}/#{path}",
      body,
      headers.merge(default_headers)
    )
  end
end
