# Ruby RSpec for API Automation

Example code base for testing and interacting with REST API's using Ruby's RSpec.

Also implements code from another project, `Ruby API Interface` to be the base class for implementing against other APIs.

## Getting started
If you don't already have Ruby installed, here is a great place to start to manage your Ruby Installations for *nix OS's.  
For Windows, check out URU https://rubyinstaller.org/add-ons/uru.html
